<?php
use App\Author;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/authors/havebooks', function () {
    $author = Author::has('Book')->get();
    return $author;
});

Route::get('/authors/havenobooks', function() {
    $author = Author::doesntHave('Book')->get();
    return $author;
});
